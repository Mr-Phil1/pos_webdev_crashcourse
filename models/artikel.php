<?php

class Artikel
{
    private $aid;
    private $atitel;
    private $ainhalt;
    private $abild;
    private $fk_uid;

//Todo: Konstruktor schreiben lassen

    /**
     * @return mixed
     */
    public function getAid()
    {
        return $this->aid;
    }

    /**
     * @param mixed $aid
     */
    public function setAid($aid)
    {
        $this->aid = $aid;
    }

    /**
     * @return mixed
     */
    public function getAtitel()
    {
        return $this->atitel;
    }

    /**
     * @param mixed $atitel
     */
    public function setAtitel($atitel)
    {
        $this->atitel = $atitel;
    }

    /**
     * @return mixed
     */
    public function getAinhalt()
    {
        return $this->ainhalt;
    }

    /**
     * @param mixed $ainhalt
     */
    public function setAinhalt($ainhalt)
    {
        $this->ainhalt = $ainhalt;
    }

    /**
     * @return mixed
     */
    public function getAbild()
    {
        return $this->abild;
    }

    /**
     * @param mixed $abild
     */
    public function setAbild($abild)
    {
        $this->abild = $abild;
    }

    /**
     * @return mixed
     */
    public function getFkUid()
    {
        return $this->fk_uid;
    }

    /**
     * @param mixed $fk_uid
     */
    public function setFkUid($fk_uid)
    {
        $this->fk_uid = $fk_uid;
    }

    /**
     * @param $id
     * @return Artikel
     */
    public static function getArtikelById(int $id)
    {
        //ToDo: Datenbankzugriff vereinheitlichen

        $db = 'kisscms_db';
        $dbuser = 'root';
        $dbpw = '123';
        $dbhost = 'mysql';

        $dsn = "mysql:dbname=$db;host=$dbhost";

        try {
            $pdoconn = new PDO($dsn, $dbuser, $dbpw);
            $query = 'SELECT * FROM t_articles WHERE aid=:id';
            $ps = $pdoconn->prepare($query);
            if ($ps === false) {
                //ToDo: Fehlerbehandlung mit throw new Exception
                echo "Prepared statement fehlgeschlagen";
            } else {
                $ps->bindParam(':id', $id, PDO::PARAM_INT);
                $psresult = $ps->execute();
                if ($psresult === false) {
                    //ToDo: Fehlerbehandlung mit throw new Exception
                    echo "Binden der Variabele fehlgeschlagen";
                } else {
                    // echo 'Träumchen: PS erfolgreich!';
                    $result = $ps->fetchObject();
                   // var_dump($result);
                    $artikel = new Artikel();

                    $artikel->setAid($result->aid);
                    $artikel->setAtitel($result->atitle);
                    $artikel->setAinhalt($result->acontent);
                    $artikel->setAbild($result->acontent);
                    $artikel->setFkUid($result->fk_uid);
                    //   var_dump($artikel);
                    return $artikel;
                }
            }

        } catch (PDOException $e) {
            echo "DB-Fehler: " . $e->getMessage();
        }

    }
}