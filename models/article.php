<?php

class Article
{
    private $title = 'Dummyproduct';
    private $desc = 'Some description';

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Article
     */
    public function setTitle(string $title): Article
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDesc(): string
    {
        return $this->desc;
    }

    /**
     * @param string $desc
     * @return Article
     */
    public function setDesc(string $desc): Article
    {
        $this->desc = $desc;
        return $this;
    }

    public static function getArticleById(int $id)
    {
        $db = 'kisscms_db';
        $dbuser = 'root';
        $dbpw = '';
        $dbhost = '127.0.0.1';
        $dsn = "mysql:dbname=$db;host=$dbhost";
        try {
            $pdoconn = new PDO($dsn,$dbuser,$dbpw);
            $pdoconn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            //echo "Verbunden.";
            $sql = "SELECT * FROM t_articles WHERE aid=:id";
            $ps = $pdoconn->prepare($sql);
            //var_dump($ps);
            $res = $ps->bindParam(':id', $id, PDO::PARAM_INT);
            $ps->execute();
            $result = $ps->fetchObject();
            //var_dump($result);
            $article = new Article();
            $article->setTitle($result->atitle);
            $article->setDesc($result->acontent);
            return $article;
        }
        catch (PDOException $e){
            echo "DB-Fehler: ".$e->getMessage();
            return false;
        }

    }

}
